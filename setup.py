#!/usr/bin/env python

from setuptools import setup

# The version is updated automatically with bumpversion
# Do not update manually
__version = '2.0.0'


def main():

    setup(
        name='brukeralcps',
        version=__version,
        package_dir={'brukeralcps': 'brukeralcps'},
        packages=['brukeralcps'],
        author='Lothar Krause and Sergi Blanch',
        author_email='controls-software@cells.es',
        description='Tango device servers for controlling BrukerEC PS',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://gitlab.com/tango-controls/device-servers/DeviceClasses/'
            'powersupply/BrukerALC_PS',
        requires=['tango (>=7.2.6)'],
        entry_points={
            'console_scripts': [
                'BrukerALC_PS = brukeralcps.BrukerALC_PS:main',
            ],
        },
    )


if __name__ == "__main__":
    main()
